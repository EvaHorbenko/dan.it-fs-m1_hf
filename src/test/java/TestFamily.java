import org.example.family.Family;
import org.example.human.Human;
import org.junit.jupiter.api.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import java.util.Arrays;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class TestFamily {

    public static void main(String[] args) {
        JUnitCore runner = new JUnitCore();
        Result result = runner.run(TestFamily.class);
    }

    private static Family family;

    @BeforeAll
    public static void init() {
        family = new Family(new Human(), new Human());
        Human[] children = new Human[5];
        int testChildrenCount = 3;
        for (int i = 0; i < testChildrenCount; i++) {
            children[i] = new Human();
        }
        family.setChildren(children);
    }

    @AfterAll
    public static void fini() {
        family = null;
    }

    @Test
    public void deleteChildByHuman() {
        Human child = new Human();
        Family controlFamily = family;

        boolean isDeleted = family.deleteChildByHuman(child);

        if(isDeleted) {
            assertThat(family.getChildren())
                    .doesNotContain(child);
        } else {
            assertThat(controlFamily)
                    .isEqualTo(family);
        }

    }

    @Test
    public void deleteChildByIdx() {
        int idx = 2;
        Family controlFamily = family;

        family.deleteChildByIdx(idx);
        Human[] refactoredChildrenArray = family.getChildren();


        if(idx < family.getChildren().length) {
            assertThat(refactoredChildrenArray[idx])
                    .isEqualTo(null);
        } else {
            assertThat(controlFamily)
                    .isEqualTo(family);
        }
    }

    @Test
    public void addChild() {
        Human newChild = new Human();
        family.addChild(newChild);

        assertThat(family.getChildren())
                .contains(newChild);
    }

    @Test
    public void countFamilyMembers() {

        Human[] children = family.getChildren();
        int childrenCount = 0;
        for (Human child : children) {
            childrenCount += (child != null) ? 1 : 0;
        }
        int familyMembersCount = 2 + childrenCount;

        assertThat(family.getFamilyMembersCount())
                .isEqualTo(familyMembersCount);
    }

}
