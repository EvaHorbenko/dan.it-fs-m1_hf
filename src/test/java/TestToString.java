import org.example.family.Family;
import org.example.human.Human;
import org.example.pet.Pet;
import org.junit.jupiter.api.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestToString {

    public static void main(String[] args) {
        JUnitCore runner = new JUnitCore();
        Result result = runner.run(TestToString.class);

        System.out.println("run tests:      " + result.getRunCount());
        System.out.println("failure tests:  " + result.getFailureCount());
        System.out.println("passed tests:   " + (result.getRunCount() - result.getFailureCount()));
    }

    private static Pet pet;
    private static Human human;
    private static Family family;

    @BeforeAll
    public static void init() {
        System.out.println("Before check");
        pet = new Pet();
        human = new Human();
        family = new Family(new Human(), new Human());
    }

    @AfterAll
    public static void fini() {
        System.out.println("After check");
        pet = null;
        human = null;
        family = null;
    }

    @Test
    public void Pet() {
        System.out.println("Test Pet");
        String stringedPet = pet.getSpecies() + "{" +
                "nickname='" + pet.getNickname() + '\'' +
                ", age=" + pet.getAge() +
                ", trickLevel=" + pet.getTrickLevel() +
                ", habits=" + Arrays.toString(pet.getHabits()) +
                '}';
        assertThat(pet.toString())
                .isEqualTo(stringedPet);
    }

    @Test
    public void Human() {
        System.out.println("Test Human");
        String stringedHuman = "Human{" +
                "name='" + human.getName() + '\'' +
                ", surname='" + human.getSurname() + '\'' +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", schedule=" + Arrays.toString(human.getSchedule()) +
                '}';
        assertThat(human.toString())
                .isEqualTo(stringedHuman);
    }

    @Test
    public void Family() {
        System.out.println("Test Family");
        String stringedFamily = "Family{" +
                "mother=" + family.getMother().toString() +
                ", father=" + family.getFather().toString() +
                ", children=" + Arrays.toString(family.getChildren()) +
                ", pet=" + family.getPet() +
                '}';
        assertThat(family.toString())
                .isEqualTo(stringedFamily);
    }
}
