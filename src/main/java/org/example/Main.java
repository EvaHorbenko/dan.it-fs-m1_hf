package org.example;

import org.example.family.Family;
import org.example.human.Human;
import org.example.pet.Pet;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human();
        Human father = new Human();
        createFamily(mother, father);

        // visualizeFinaliseCalling();
        createScheduleFor(father);
        createScheduleFor(mother);
    }

    private static Family[] families = new Family[50];

    private static void createFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        for (int i = 0; i < families.length; i++) {
            if(families[i] == null) {
                families[i] = newFamily;
                families[i].setPet(new Pet());

//                families[i].getMother().greetPet();
                families[i].addChild(new Human());
                families[i].deleteChildByIdx(0);

                break;
            }
        }
    }

    private static void visualizeFinaliseCalling() {
        System.out.println("is called");
        for (int i = 0; i < 70000; i++) {
            new Pet();
        }
    }

    private static void createScheduleFor(Human human) {
        String[][] schedule = new String[7][2];
        for (int i = 0; i < Human.DayOfWeek.values().length; i++) {
            int n = (int) (Math.random() * Human.activities.length);
            String randomActivity = Human.activities[n];
            schedule[i][0] = (Human.DayOfWeek.values()[i]).name();
            schedule[i][1] = randomActivity;

//            System.out.printf("%15s : %s \n", schedule[i][0], schedule[i][1]);
        }
        human.setSchedule(schedule);
        System.out.println();
    }

}