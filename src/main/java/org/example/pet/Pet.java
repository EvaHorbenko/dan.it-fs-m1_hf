package org.example.pet;

import java.util.Arrays;
import java.util.Objects;

public class Pet {

    public enum Species {
        dog,
        cat,
        squirrel,
        frog,
        rat,
        owl,
        snake
    }

    private Species     species;
    private String      nickname;
    private double      age;
    private int         trickLevel;
    private String[]    habits;

    // constructor empty
    public Pet() {
        this.setSpecies(Species.dog);
        this.setNickname("Shelly");
        this.setAge(4.4);
        this.setTrickLevel(75);
        this.setHabits(new String[]{"run for squirrels", "eat", "sleep", "play"});

        new Pet(this.species, this.nickname);
    }

    // constructor 2values
    public Pet(Species species, String nickname) {
        this.setAge(4.4);
        this.setTrickLevel(75);
        this.setHabits(new String[]{"run for squirrels", "eat", "sleep", "play"});

        new Pet(species, nickname, this.age, this.trickLevel, this.habits);
    }

    // constructor full
    public Pet(Species species, String nickname, double age, int trickLevel, String[] habits) {
        this.setSpecies(species);
        this.setNickname(nickname);
        this.setAge(age);
        this.setTrickLevel(trickLevel);
        this.setHabits(habits);
    }


    // getters / setters
    public Species getSpecies() {
        return species;
    }
    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public double getAge() {
        return age;
    }
    public void setAge(double age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    // methods

    private static void eat() {
        System.out.println("I'm eating!");
    }
    private void respond() {
        System.out.printf("Hello Master! I'm %s. I miss you... \n", this.nickname);
    }

    private static void foul() {
        System.out.println("I should cover my tracks well...");
    }


    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return trickLevel == pet.trickLevel && species.equals(pet.species) && nickname.equals(pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, trickLevel);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Pet Finalize was called \n" + this.toString());
    }
}

