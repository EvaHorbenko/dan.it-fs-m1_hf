package org.example.human;

import org.example.family.Family;
import org.example.pet.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Human {

    public enum DayOfWeek {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday;
    }

    public static String[] activities = {"talk to the duck to resolve all problem", "go to the river", "go to the forest", "learn german", "learn spanish", "learn english", "learn programming", "watch movie", "spend time with family", "read book", "study", "run", "visit yoga", "rest", "being busy doing nothing", "drink bear with friend", "visit concert"};

    private String      name;
    private String      surname;
    private int         year;
    private int         iq;
    private String[][]  schedule;
    private Family      family;


    // constructor empty
    public Human() {
        this.setName("Harry");
        this.setSurname("Potter");
        this.setYear(1990);
        this.setIq(87);
        this.setSchedule(new String[7][2]);

        new Human(this.name, this.surname, this.year, this.iq, this.schedule);
    }

    // constructor 3values
    public Human(String name, String surname, int year) {
        this.setIq(87);
        this.setSchedule(new String[7][2]);

        new Human(name, surname, year, this.iq, this.schedule);
    }

    // constructor full
    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.setName(name);
        this.setSurname(surname);
        this.setYear(year);
        this.setIq(iq);
        this.setSchedule(schedule);
    }


    // getters / setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }


    // methods
    public void greetPet() {
        System.out.printf("Hi, %s! \n", this.family.getPet().getNickname());
    }

    public void describePet() {
        Pet.Species species = this.family.getPet().getSpecies();
        double      age     = this.family.getPet().getAge();
        String      tricky  = this.family.getPet().getTrickLevel() > 50 ? "very tricky" : "almost not tricky";

        System.out.printf("I have a %s, he is %d years old, he is %s.", species, age, tricky);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, iq, family);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return iq == human.iq && name.equals(human.name) && surname.equals(human.surname) && family.equals(human.family);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human Finalize was called \n" + this.toString());
        super.finalize();
    }
}

