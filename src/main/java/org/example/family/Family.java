package org.example.family;

import org.example.human.Human;
import org.example.pet.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    private Human   mother;
    private Human   father;
    private Human[] children;
    private Pet     pet;


    // constructor
    public Family(Human mother, Human father) {
        this.setMother(mother);
        mother.setFamily(this);
        this.setFather(father);
        father.setFamily(this);

        this.setChildren(new Human[5]);
        this.setPet(new Pet(Pet.Species.owl, "Hedwig", 7, 96, new String[]{"fly", "sleep", "bring post"}));
    }


    // getters / setters
    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }
    public void setChildren(Human[] children) {
        this.children = children;
        for (Human child : children) {
            if (child != null) {
                child.setFamily(this);
            }
        }
    }

    public Pet getPet() {
        return pet;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
    }


    // methods
    public void addChild(Human child) {

        Human newChild = child;
        for (int i = 0; i < children.length; i++) {
            if(children[i] == null) {
                children[i] = newChild;
                newChild.setFamily(this);
                break;
            }
        }
    }

    public boolean deleteChildByHuman(Human child) {
        for (int i = 0; i < children.length; i++) {
            if(children[i] != null && children[i].equals(child)) {
                children[i] = null;
                return true;
            }
        }
        return false;
    }

    public boolean deleteChildByIdx(int idx) {
        if(children[idx] != null) {
            children[idx] = null;
            return true;
        }
        return false;
    }

    public int getFamilyMembersCount() {
        int count = 2; // minimum mother and father are in family
        for (int i = 0; i < children.length; i++) {
            if(children[i] != null) {
                count++;
            }
        }
        return count;
    }


    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother.toString() +
                ", father=" + father.toString() +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Family Finalize was called \n" + this.toString());
        super.finalize();
    }
}
